module.exports = {
	server: {
		port: 8080
    },
    /*
    ** Headers of the page
    */
    head: {
        title: 'keke-cracker',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'HTTP BruteForce Tool' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: { color: '#3B8070' },
    /*
    ** Build configuration
    */
    buildModules: [
        '@nuxtjs/vuetify'
    ],
    
    build: {
        /*
        ** Run ESLint on save
        */
        extend (config, { isDev, isClient }) {
        }
    }
}

